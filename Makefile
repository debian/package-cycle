FILES=package-cycle.svg archive-software.svg

.PHONY: all
all: $(FILES)

%.svg: %.dot
	dot -Tsvg $< > $@~
	scour --create-groups --no-renderer-workaround \
		--strip-xml-prolog --remove-descriptive-elements \
		--enable-comment-stripping --indent=none \
		--enable-id-stripping --shorten-ids \
		-i $@~ -o $@
	rm $@~

$(FILES:%.svg=preview-%): preview-%: %.dot
	dot -Tx11 $<

.PHONY: clean
clean:
	rm -f $(FILES)
