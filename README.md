Debian workflow graphics
========================

This repository holds graphics depicting the workflow of the Debian
archive through various aspects.

The `package-cycle.dot` graph describes the flow of packages from
maintainers into the various queues. The objective is to allow users
and developers fairly familiar with Debian's unstable, testing and
stable suite to understand the flow of packages between the suite. The
graph also aims to help people discover other suites and how they
interact with each other.

The `archive-software.dot` graph describes in details the interactions
of all the major software daemons, cronjobs and scripts that make the
whole archive work correctly. It is mostly aimed at Debian maintainers
trying to figure out where their packages are in the archive.

Thanks
------

Thanks to Martin F. Krafft for inspiration. The original Dia graph and
source code and other credits are still available in the history of
this git repo.

Also thanks to the friendly people of #debian-buildd, #debian-release
and #debian-www for review and corrections. Specifically, thanks to,
in no particular order:

 * Paul R. Tagliamonte
 * Marc 'HE' Brockschmidt
 * Kurt Roeckx
 * Aurelien Jarno
 * Adam D. Barratt
 * David Prévot
 * Samuel Thibault
 * Niels Thykier
 * Andreas Barth
 * Paul Wise

Copyright
---------

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" />][cc-by-sa-4]
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa-4].

[cc-by-sa-4]: http://creativecommons.org/licenses/by-sa/4.0/
